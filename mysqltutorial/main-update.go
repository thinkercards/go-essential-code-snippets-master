package main

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/go-sql-driver/mysql"
)

func main() {

	db, err := sql.Open("mysql", "root:progno123.@tcp(127.0.0.1:3306)/northwind")
	defer db.Close()

	if err != nil {
		log.Fatal(err)
	}

	sql := "update privileges set privilege_name='test 6767' WHERE id in (6,7)"
	res, err := db.Exec(sql)

	if err != nil {
		panic(err.Error())
	}

	affectedRows, err := res.RowsAffected()

	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("The statement affected %d rows\n", affectedRows)
}
