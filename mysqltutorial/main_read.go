package main

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/go-sql-driver/mysql"
)

type Privileges struct {
	Id             int
	privilege_name string
}

func main() {

	db, err := sql.Open("mysql", "root:progno123.@tcp(127.0.0.1:3306)/northwind")
	defer db.Close()

	if err != nil {
		log.Fatal(err)
	}

	log.Output(1, "Block one")

	res, err := db.Query("select * from privileges")
	defer res.Close()

	if err != nil {
		log.Fatal(err)
	}
	log.Output(1, "Block two")

	for res.Next() {

		var objModel Privileges

		err := res.Scan(&objModel.Id, &objModel.privilege_name)

		if err != nil {
			log.Fatal(err)
		}

		fmt.Printf("%v\n", objModel)
	}

	log.Output(1, "Block three")
}
